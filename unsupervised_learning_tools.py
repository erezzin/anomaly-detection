import numpy as np
import scipy as sp
import pandas as pd
import tensorflow as tf
from scipy.spatial.distance import cdist
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()


class PointAssociation:
    def __init__(self, all_points, mask):
        self.association_mask = mask
        self.associated_points = all_points[mask]

def plot_clusters(points, cluster_associations):
    #ensure form
    cluster_associations = cluster_associations.astype(np.int)\
        .reshape(max(cluster_associations.shape))

    if cluster_associations is None or len(points) != len(cluster_associations):
        raise Exception()

    cmap = [
        'red',
        'green',
        'yellow',
        'blue',
        'purple',
        'orange',
        'aquamarine',
        'lime',
        'tan',
        'gold',
        'navy',
        'springgreen',
        'grey',
        'brown',
    ]
    colors = [None] * len(points)
    for i in range(len(points)):
        colors[i] = cmap[cluster_associations[i] % len(cmap)]

    plt.scatter(points[:, 0], points[:, 1], color=colors)
    plt.pause(.005)


def get_connected_components_association(points, **dbscan_params):
    return DBSCAN(**dbscan_params).fit_predict(points)


def find_elbow(total_distance_generator):
    distances = np.array(total_distance_generator)
    der2 = distances[2:] + distances[:-2] - 2 * distances[1:-1]
    # if np.min(der2) / np.max(der2) < .1:
    #     return 0
    return np.argmax(der2) + 1


def get_set_diameter(X, metric = "euclidean"):
    return np.max(cdist(X, X, metric))


def get_components_by_associations(points, assoc):
    components = {}

    unique_labels = np.unique(assoc)
    for lbl in sorted(unique_labels):
        mask_in_points = assoc == lbl
        components[lbl] = PointAssociation(points, mask_in_points)

    return components
