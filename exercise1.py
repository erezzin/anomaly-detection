import numpy as np
import pandas as pd
from sklearn.metrics import auc
from sklearn.mixture import GaussianMixture
import matplotlib.pyplot as plt
import seaborn as sns

import unsupervised_learning_tools as tools

sns.set()

def get_normal_density_value(p, mean, cov_mat):
    p1 = p - mean
    sig = np.sqrt(cov_mat.diagonal())
    rho = cov_mat[1,0] / (sig[0] * sig[1])
    z = np.exp(-(((p1 / sig) * (p1 / sig)).sum(axis=1) - 2 * rho * np.prod(p1 / sig,axis=1)) / 2)
    return z


if __name__ == "__main__":
    df = pd.read_csv("machine_inspection.txt", sep='     ', header=None)
    X = df.values

    # epss = [7, 8, 9]
    # smpls = [7, 8, 9]
    # plt.figure()
    #
    # for i, eps in enumerate(epss):
    #     continue
    #     for j, min_samples in enumerate(smpls):
    #         plt.subplot(len(epss), len(smpls), 1 + i * len(smpls) + j)
    #         plt.title("eps = {}, smpls = {}".format(eps, min_samples))
    #         assoc = tools.get_connected_components_association(X, eps=eps, min_samples=min_samples)
    #         tools.plot_clusters(df.values, assoc)
    # # plt.show()

    eps = 8
    min_samples = 9

    assoc = tools.get_connected_components_association(X, eps=eps, min_samples=min_samples)
    lbl_to_comp_and_mask = tools.get_components_by_associations(X, assoc)

    em = GaussianMixture(2, means_init=[lbl_to_comp_and_mask[0].associated_points.mean(axis=0),
                                        lbl_to_comp_and_mask[1].associated_points.mean(axis=0)])
    em.fit(X)

    # plt.figure()
    x = X[:, 0]
    y = X[:, 1]
    z = 0
    for i in range(2):
        mean = em.means_[i]
        cov = em.covariances_[i]
        z = np.maximum(get_normal_density_value(X, mean, cov), z)

    plt.figure()

    plt.scatter(x, y, c=z, cmap="YlOrRd")

    plt.scatter(em.means_[:,0], em.means_[:,1])

    plt.figure()

    threshold = .146

    anomaly_assoc = (z < threshold) * 1

    tools.plot_clusters(X, anomaly_assoc)

    plt.show()

    pass
